import xml.etree.ElementTree as ET


def fetchPositionData(filePath):
    tree = ET.parse(filePath)
    root = tree.getroot()
    # print(root)
    index = 1
    train = []
    test = []
    for child in root:
        new_data = []
        if child.tag == 'trainingExample':
            for coord in child:
                new_data.append(coord.attrib)
            if index % 2 == 0:
                train.append(new_data)
            else:
                test.append(new_data)
            index += 1
        # print(train)
        # print(test)

    train_data = []
    for i in range(0, len(train)):
        x = []
        y = []
        for j in range(0, len(train[i])):
            x.append(int(float(train[i][j]['x'])))
            y.append(int(float(train[i][j]['y'])))
        train_data.append([x, y])
    return train_data



# fetchPositionData()
